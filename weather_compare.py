"""
Statistics for comparing different climate datasets

Must install eofs
    pip install eofs
"""
import os
import itertools
import StringIO
import numpy as np
from time import time
from datetime import datetime
from scipy.stats import pearsonr, norm
from scipy.io.netcdf import netcdf_file
from eofs.standard import Eof
from weather import SITE_COLUMNS, CSIP_SOURCE_NAMES, CHUNK_SIZE, MODEL_NAMES, retrieve_data, convert_output_data_to_matrices_by_variable, get_key_from_site


def check_length(data, n):
    if len(data) != n:
        raise Exception('Observed and modeled (and dates) datasets must be the same size!')


def check_sizes(obs, mod):
    if obs.shape != mod.shape:
        raise Exception('The shapes of the two datasets (observed and modeled) must be the same!')


def spatial_reducer(func, all_observed, all_modeled, method='flatten', **kwargs):
    check_sizes(all_observed, all_modeled)
    if method == 'flatten':
        return func(all_observed.flatten(), all_modeled.flatten(), **kwargs)
    elif method == 'spatial_mean_before':
        return func(all_observed.mean(axis=1), all_modeled.mean(axis=1), **kwargs)
    elif method == 'spatial_mean_after':
        return np.array([func(all_observed[:, j], all_modeled[:, j], **kwargs) for j in range(all_observed.shape[1])]).mean()
    else:
        raise Exception('Method "{0}" not recognized!'.format(method))


def nrmse(observed, modeled):
    n = len(observed)
    check_length(modeled, n)
    if n <= 1:
        raise Exception('Must have greater than one data point!')
    num = np.sqrt(((modeled - observed) ** 2).sum() / n)
    den = np.sqrt(((observed - observed.mean()) ** 2).sum() / (n - 1))
    return num / den


def correl(observed, modeled):
    check_sizes(observed, modeled)
    return pearsonr(observed, modeled)[0]


def estimate_trend(x):
    n_vals = len(x)
    i, j = zip(*[c for c in itertools.combinations(range(n_vals), 2)])
    i = np.array(i)
    j = np.array(j)
    return np.median((x[j] - x[i])/(j-i))


def mann_kendall(x, alpha = 0.05, extra_vals=False):
    """
    Created on Thu Dec 29 15:24:08 2011
    http://pydoc.net/Python/ambhas/0.4.0/ambhas.stats/

    @author: Sat Kumar Tomer
    @website: www.ambhas.com
    @email: satkumartomer@gmail.com

    this perform the MK (Mann-Kendall) test to check if there is any trend present in
    data or not

    Input:
        x:   a vector of data
        alpha: significance level

    Output:
        trend: tells the trend (increasing, decreasing or no trend)
        h: True (if trend is present) or False (if trend is absence)
        p: p value of the sifnificance test
        z: normalized test statistics

    Examples
    --------
      >>> x = np.random.rand(100)
      >>> trend,h,p,z = mann_kendall(x,0.05)
    """
    n = len(x)

    # calculate S
    s = 0
    for k in xrange(n-1):
        for j in xrange(k+1,n):
            s += np.sign(x[j] - x[k])

    # calculate the unique data
    unique_x = np.unique(x)
    g = len(unique_x)

    # calculate the var(s)
    if n == g: # there is no tie
        var_s = (n*(n-1)*(2*n+5))/18
    else: # there are some ties in data
        tp = np.zeros(unique_x.shape)
        for i in xrange(len(unique_x)):
            tp[i] = sum(unique_x[i] == x)
        var_s = (n*(n-1)*(2*n+5) + np.sum(tp*(tp-1)*(2*tp+5)))/18

    if s>0:
        z = (s - 1)/np.sqrt(var_s)
    elif s == 0:
        z = 0
    elif s<0:
        z = (s + 1)/np.sqrt(var_s)

    # calculate the p_value
    p = 2*(1-norm.cdf(abs(z))) # two tail test
    h = abs(z) > norm.ppf(1-alpha/2)

    if (z<0) and h:
        trend = 'decreasing'
    elif (z>0) and h:
        trend = 'increasing'
    else:
        trend = 'no trend'

    if extra_vals:
        return trend, h, p, z
    else:
        return z


def calc_eofs(data, n_modes, verbose=False):

    # create a solver class, taking advantage of built-in weighting
    solver = Eof(data)

    # retrieve the first two EOFs from the solver class
    var_frac = solver.varianceFraction()[:n_modes]
    eofs = solver.eofs(neofs=n_modes)
    eofs = tuple(eofs)
    if verbose:
        print eofs[0][0, 3], eofs[1][0, 3], eofs[0].sum(), eofs[1].sum()
    return eofs, var_frac


def calc_eofs_from_netcdf(file_path, variable_name, n_modes, slices=slice(None), verbose=False):
    # read a spatial-temporal field, time must be the first dimension
    ncin = netcdf_file(file_path)
    v = ncin.variables[variable_name]
    if not hasattr(slices, '__len__'):
        data = v[slices]
    elif len(slices) == 1:
        data = v[slices[0]]
    elif len(slices) == 2:
        data = v[slices[0], slices[1]]
    elif len(slices) == 3:
        data = v[slices[0], slices[1], slices[2]]
    ncin.close()
    return calc_eofs(data, n_modes, verbose=verbose)


def get_probabilities(data, bins=100):
    hist, bin_edges = np.histogram(data, bins=bins, density=True)
    P = hist * np.diff(bin_edges)
    return P


def brier_score(observed, modeled, bins=100):
    Po = get_probabilities(observed, bins=bins)
    Pm = get_probabilities(modeled, bins=bins)
    return ((Pm - Po)**2).mean()


def significance_score(observed, modeled, bins=100):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    Po = get_probabilities(observed, bins=bins)
    Pm = get_probabilities(modeled, bins=bins)
    return np.minimum(Po, Pm).sum()


def rel_err(observed, modeled):
    check_sizes(observed, modeled)
    obs_mean = observed.mean()
    return abs(modeled.mean() - obs_mean) / abs(obs_mean)


def all_nrmse(all_observed, all_modeled, method='flatten'):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """

    return spatial_reducer(nrmse, all_observed, all_modeled, method=method)


def correl_across_months(all_observed, all_modeled, dates):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """

    observed = all_observed.mean(axis=1)
    modeled = all_modeled.mean(axis=1)
    n = len(observed)
    check_length(modeled, n)
    check_length(dates, n)
    month_indices = [d.month - 1 for d in dates]
    n_count = np.bincount(month_indices, minlength=12)

    # get long-term monthly means
    monthly_mean_observed = np.bincount(month_indices, weights=observed, minlength=12) / n_count
    monthly_mean_modeled = np.bincount(month_indices, weights=modeled, minlength=12) / n_count

    # correlations
    return correl(monthly_mean_observed, monthly_mean_modeled)


def correl_across_space(all_observed, all_modeled):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    return correl(all_observed.mean(axis=0), all_modeled.mean(axis=0))


def rel_mann_kendall(all_observed, all_modeled, alpha=0.05):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    check_sizes(all_observed, all_modeled)
    obs = np.array([mann_kendall(all_observed[:, j], alpha=alpha) for j in range(all_observed.shape[1])])
    mod = np.array([mann_kendall(all_modeled[:, j], alpha=alpha) for j in range(all_modeled.shape[1])])
    return rel_err(obs, mod)


def rel_trend_estimate(all_observed, all_modeled):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    check_sizes(all_observed, all_modeled)
    obs = np.array([estimate_trend(all_observed[:, j]) for j in range(all_observed.shape[1])])
    mod = np.array([estimate_trend(all_modeled[:, j]) for j in range(all_modeled.shape[1])])
    return rel_err(obs, mod)


def rel_err_eof(all_observed, all_modeled, n_modes, verbose=False):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    check_sizes(all_observed, all_modeled)
    obs_eof, obs_explained_var = calc_eofs(all_observed, n_modes)
    mod_eof, mod_explained_var = calc_eofs(all_modeled, n_modes)
    if verbose:
        obs_var = ['%%%.1f' % (v * 100) for v in obs_explained_var]
        mod_var = ['%%%.1f' % (v * 100) for v in mod_explained_var]
        print 'Explained variance: observed ({0}), modeled ({1})'.format(', '.join(obs_var), ', '.join(mod_var))
    return tuple([rel_err(obs_eof[i], mod_eof[i]) for i in range(n_modes)])


def all_brier_score(all_observed, all_modeled, bins=100, method='flatten'):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    return spatial_reducer(brier_score, all_observed, all_modeled, method=method, bins=bins)


def all_significance_score(all_observed, all_modeled, bins=100, method='flatten'):
    """
    all_observed and all_modeled are 2D arrays with shape = [len(dates), n_locations]
    """
    return spatial_reducer(significance_score, all_observed, all_modeled, method=method, bins=bins)


def all_scores(models_all_observed, models_all_modeled, dates):
    """
    all_data is a dictionaries with model names as keys and NamedData objects as values
    """
    output = {}
    for model in models_all_observed:
        all_observed = models_all_observed[model]
        all_modeled = models_all_modeled[model]
        output[model] = {
            'mean': rel_err(all_observed, all_modeled)
#             'std': rel_err(all
        }
    return output


class ModelScorer:

    def __init__(self, model_names, dates, aggregation_temporal='sum', aggregation_spatial='flatten', verbose=False, n_eof_modes=2):

        if not isinstance(model_names, list):
            raise Exception('The "model_names" parameter must be a list of model names!')
        if not isinstance(dates, list):
            raise Exception('The "dates" parameter must be a list of dates! Is of type {0}'.format(type(dates)))

        self.model_names = [str(m) for m in model_names]
        self.dates = MonthCounter.get_output_monthly_dates(dates)
        self.observed_data = None
        self.model_data = {}
        self.method = aggregation_spatial
        self.aggregate_type = aggregation_temporal
        self.verbose = verbose
        self.n_modes = n_eof_modes
        self.weights = {
            'mean': 1,
            'std': 1,
            'nrmse': 1,
            'correl_months': 1,
            'correl_space': 1,
            'mann_kendall_z': 0.5,
            'trend_estimate': 0.5,
            'brier_score': 0.5,
            'significance_score': 0.5
        }
        for i in range(n_eof_modes):
            self.weights['eof{0}'.format(i)] = 0.5


    def check_ready(self):
        if self.observed_data is None:
            raise Exception('Observed data must be set in order to calculate statistics')


    def add_data(self, name, number, dates, data):

        return self.add_named_data(NamedData(name, number, dates, data))


    def add_named_data(self, named_data):

        if not isinstance(named_data, NamedData):
            raise Exception('The "named_data" parameter must be a NamedData object!')

        if named_data.name not in self.model_names and named_data.name.lower() != 'observed':
            raise Exception('The "name" parameter ("{0}") must be "observed" or one the specified model names ({1})'.format(named_data.name, ', '.join(self.model_names)))

        named_data = named_data.monthly(aggregate_type=self.aggregate_type)
        if named_data.dates != self.dates:
            raise Exception('The "dates" parameter must be the same across all different models.\n\nExpecting:\n{0}\n\nGot:\n{1}'.format(self.dates, named_data.dates))

        if named_data.is_observed:
            self.observed_data = named_data
        else:
            self.model_data[named_data.name] = named_data
        return named_data


    def rel_err_mean(self, name):
        return rel_err(self.observed_data.data, self.model_data[name].data)


    def rel_err_std(self, name):
        return rel_err(self.observed_data.data.std(), self.model_data[name].data.std())


    def nrmse(self, name):
        return all_nrmse(self.observed_data.data, self.model_data[name].data, method=self.method)


    def correl_temporal(self, name):
        o = self.observed_data.mean_across_space().monthly_means(aggregate_type=self.aggregate_type)
        m = self.model_data[name].mean_across_space().monthly_means(aggregate_type=self.aggregate_type)
        return correl(o.data.flatten(), m.data.flatten())


    def correl_space(self, name):
        o = self.observed_data.yearly_means(aggregate_type=self.aggregate_type)
        m = self.model_data[name].yearly_means(aggregate_type=self.aggregate_type)
        return correl(o.data.flatten(), m.data.flatten())


    def rel_mann_kendall(self, name, alpha=0.05):
        return rel_mann_kendall(self.observed_data.data, self.model_data[name].data, alpha=alpha)


    def rel_trend_estimate(self, name):
        return rel_trend_estimate(self.observed_data.data, self.model_data[name].data)


    def rel_err_eof(self, name, n_modes=2):
        return rel_err_eof(self.observed_data.data, self.model_data[name].data, n_modes)


    def brier_score(self, name, bins=100):
        return all_brier_score(self.observed_data.data, self.model_data[name].data, bins=bins, method=self.method)


    def significance_score(self, name, bins=100):
        return all_significance_score(self.observed_data.data, self.model_data[name].data, bins=bins, method=self.method)


    def stats(self, name, alpha=0.05, bins=100):
        if self.verbose:
            print '  - model {0}'.format(name)
        if self.observed_data.location_count() < 2:
            raise Exception('Must incorporate more than 1 location!')
        d = {
            'mean': self.rel_err_mean(name),
            'std': self.rel_err_std(name),
            'nrmse': self.nrmse(name),
            'correl_months': -self.correl_temporal(name),
            'correl_space': -self.correl_space(name),
            'mann_kendall_z': self.rel_mann_kendall(name, alpha=alpha),
            'trend_estimate': self.rel_trend_estimate(name),
            'brier_score': self.brier_score(name, bins=bins),
            'significance_score': -self.significance_score(name, bins=bins)
        }

        eofs = self.rel_err_eof(name, n_modes=self.n_modes)
        for i, eof in zip(range(len(eofs)), eofs):
            d['eof{0}'.format(i)] = eof

        return d


    def all_stats(self, alpha=0.05, bins=100):
        output = {}
        for name in self.model_data:
            output[name] = self.stats(name, alpha=alpha, bins=bins)
        return output


    def stat_matrix(self, alpha=0.05, bins=100):

        # get stats
        stats = self.all_stats(alpha=alpha, bins=bins)
        model_names = sorted(stats.keys())
        stat_names = sorted(next(stats[s] for s in stats).keys())

        # convert to matrix with shape (n_models, n_stats)
        return model_names, stat_names, np.array([[stats[model_name][stat_name] for stat_name in stat_names] for model_name in model_names])


    def all_scores(self, alpha=0.05, bins=100):

        # get stats
        model_names, stat_names, matrix = self.stat_matrix(alpha=alpha, bins=bins)

        # get weights
        weights = np.array([self.weights[name] for name in stat_names])

        # calculate scores
        mins = matrix.min(axis=0)
        maxs = matrix.max(axis=0)
        scores = (matrix - mins) / (maxs - mins) * 9.0
        totals = (scores * weights).sum(axis=1)

        # collect output
        output = {}
        for m, s in zip(model_names, totals):
            output[m] = s
        return model_names, stat_names, matrix, output


    def stat_table(self, alpha=0.05, bins=100, delimiter=None):

        s = time()
        model_names, stat_names, matrix, scores = self.all_scores(alpha=alpha, bins=bins)
        if self.verbose:
            print '  Calculate all scores : {0} seconds'.format(time() - s)
            s = time()
        ordered_scores = np.array([scores[model_name] for model_name in model_names]).reshape((-1, 1))
        if self.verbose:
            print '  Order the scores     : {0} seconds'.format(time() - s)
            s = time()
        data = [['Model'] + stat_names + ['RS Score']]
        data.extend([[model_name] + list(row) + list(score) for model_name, row, score in zip(model_names, matrix, ordered_scores)])
        if self.verbose:
            print '  Build out data list  : {0} seconds'.format(time() - s)
            s = time()
        if delimiter is not None:
            f = StringIO.StringIO()
            if delimiter == ',':
                lines = [delimiter.join(['"{0}"'.format(d) if isinstance(d, str) else str(d) for d in row]) for row in data]
            else:
                lines = [delimiter.join([str(d) for d in row]) for row in data]
            f.write('\n'.join(lines))
            fstr = f.getvalue()
            if self.verbose:
                print '  Write to string      : {0} seconds'.format(time() - s)
                s = time()
            f.close()
            return fstr
        return data


class NamedData:
    """
    Contains modeled or observed data with the following information
        - model number
        - model name
        - dates
        - 2D arrays (with shape [n_time, n_locations]) as values
    """

    def __init__(self, name, number, dates, data):

        if not isinstance(name, str):
            raise Exception('The "name" parameter must be of str type!')
        if not isinstance(number, int):
            raise Exception('The "number" parameter must be of int type!')
        if not isinstance(dates, list) or len(dates) <= 0:
            raise Exception('The "dates" parameter must be a list of dates with more than zero elements!')
        if data is not None and (not isinstance(data, np.ndarray) or not data.ndim == 2):
            raise Exception('The "data" parameter must be of ndarray type and with shape (n_time, n_locations)')
        if data.shape[0] != len(dates):
            raise Exception('The "data" parameter must be the same size as dates ({0})... Got {1} for {2}'.format(len(dates), data.shape[0], name))

        self.name = name
        self.is_observed = self.name.lower() == 'observed'
        self.number = number
        self.dates = dates
        self.data = data

    def add_location(self, data):

        if data is None:
            raise Exception('Cannot add None data object!')
        if not isinstance(data, np.ndarray) or not data.ndim == 2:
            raise Exception('The "data" parameter must be of ndarray type and with shape (n_time, n_locations)')
        if data.shape[0] != len(self.dates):
            raise Exception('The "data" parameter must have the same number of elements in the first dimension as the dates list')

        if self.data is None:
            self.data = data
        else:
            self.data = np.hstack((self.data, data))

    def location_count(self):
        if self.data is None:
            return 0
        return self.data.shape[1]

    def time_count(self):
        if self.data is None:
            return 0
        return self.data.shape[0]

    def check(self, check_names, check_dates):

        if self.name not in check_names and self.name.lower() != 'observed':
            raise Exception('The "name" parameter must be "observed" or one the specified model names ({0})'.format(', '.join(check_names)))

        if self.dates != check_dates:
            raise Exception('The "dates" parameter must be the same across all different models')

    def monthly(self, aggregate_type='sum'):

        # month indices
        mc = MonthCounter(self.dates[0])
        month_indices = [mc.get_num(d) for d in self.dates]

        # output dates
        months = sorted(list(set(month_indices)))
        output_dates = [mc.get_date(m) for m in months]

        # sums
        output_data = np.array([np.bincount(month_indices, weights=self.data[:, j]) for j in range(self.location_count())])
        if aggregate_type == 'mean':
            output_data = output_data / np.bincount(month_indices)
        elif aggregate_type != 'sum':
            raise Exception('Can only take the sum or mean of data!')
        return NamedData(self.name, self.number, output_dates, output_data.T)

    def monthly_means(self, aggregate_type='sum'):

        m = self.monthly(aggregate_type=aggregate_type)

        # month indices
        month_indices = [d.month - 1 for d in m.dates]
        output_dates = [datetime(self.dates[0].year, m + 1, 1) for m in range(12)]

        # sums
        output_data = np.array([np.bincount(month_indices, weights=self.data[:, j]) for j in range(self.location_count())]) / np.bincount(month_indices)
        return NamedData(self.name, self.number, output_dates, output_data.T)

    def yearly(self, aggregate_type='sum'):

        # year indices
        year_indices = [d.year - self.dates[0].year for d in self.dates]

        # output dates
        years = sorted(list(set(year_indices)))
        output_dates = [datetime(self.dates[0].year + y, 1, 1) for y in years]

        # sums
        output_data = np.array([np.bincount(year_indices, weights=self.data[:, j]) for j in range(self.location_count())])
        if aggregate_type == 'mean':
            output_data = output_data / np.bincount(year_indices)
        elif aggregate_type != 'sum':
            raise Exception('Can only take the sum or mean of data!')
        return NamedData(self.name, self.number, output_dates, output_data.T)

    def yearly_means(self, aggregate_type='sum'):
        m = self.yearly(aggregate_type=aggregate_type)
        return NamedData(self.name, self.number, [datetime(self.dates[0].year, 1, 1)], m.data.mean(axis=0).reshape((1, -1)))

    def mean_across_space(self):
        return NamedData(self.name, self.number, self.dates, self.data.mean(axis=1).reshape((-1, 1)))


class MonthCounter:

    def __init__(self, start_date):
        self.cvt = lambda date: date.year * 12 + date.month - 1
        self.inv = lambda num: datetime(int(num / 12), int(num % 12) + 1, 1)
        self.start_date = start_date
        self.start_num = self.cvt(start_date)

    def get_num(self, date):
        return self.cvt(date) - self.start_num

    def get_date(self, num):
        return self.inv(num + self.start_num)

    @staticmethod
    def get_monthly_indices(dates):
        mc = MonthCounter(dates[0])
        return [mc.get_num(d) for d in dates]

    @staticmethod
    def get_output_monthly_indices(dates):
        mc = MonthCounter(dates[0])
        month_indices = [mc.get_num(d) for d in dates]
        return sorted(list(set(month_indices)))

    @staticmethod
    def get_output_monthly_dates(dates):
        mc = MonthCounter(dates[0])
        month_indices = [mc.get_num(d) for d in dates]
        months = sorted(list(set(month_indices)))
        return [mc.get_date(m) for m in months]


def save_stats(sites_file, observed_dataset, modeled_datasets, start_date, end_date, output_dir=None, verbose=False, climate_models=None, climate_rcps=None, chunk_size=CHUNK_SIZE):

    # get observed and modeled data
    observed = retrieve_data(sites_file, observed_dataset, start_date, end_date, verbose=verbose, use_cache=True,
                             aggregate_timestep='daily', climate_models=climate_models, climate_rcps=climate_rcps,
                             chunk_size=chunk_size)
    modeled = {}
    for source in modeled_datasets:
        m = retrieve_data(sites_file, source, start_date, end_date, verbose=True, use_cache=True,
                          aggregate_timestep='daily', climate_models=climate_models, climate_rcps=climate_rcps,
                          chunk_size=chunk_size)
        modeled.update(m)

    # load observed and modeled data into objects for comparison
    dates, obs_data, obs_headers = next(observed[o] for o in observed)
    sites, obs_data_by_variable = convert_output_data_to_matrices_by_variable(obs_data, obs_headers)

    # scorer object
    pcp = ModelScorer(MODEL_NAMES, dates, aggregation_temporal='sum', verbose=verbose)
    tmpmin = ModelScorer(MODEL_NAMES, dates, aggregation_temporal='mean', verbose=verbose)
    tmpmax = ModelScorer(MODEL_NAMES, dates, aggregation_temporal='mean', verbose=verbose)

    # fill objects with observed data
    pcp.add_data('observed', -1, dates, obs_data_by_variable['apcp'])
    tmpmin.add_data('observed', -1, dates, obs_data_by_variable['tmpmin'])
    tmpmax.add_data('observed', -1, dates, obs_data_by_variable['tmpmax'])

    # load modeled
    models = [source for source in modeled]

    for i, model in zip(range(len(models)), models):

        # get names
        gcm_model = None
        gcm_rcp = None
        if '/' in model:
            source, gcm_model, gcm_rcp = model.split('/')
        else:
            source = model

        # arrange data
        mod_dates, mod_data, mod_headers = modeled[model]
        mod_sites, mod_data_by_variable = convert_output_data_to_matrices_by_variable(mod_data, mod_headers,
                                                                                      ordered_sites=sites)

        # fill objects
        pcp.add_data(gcm_model, i, mod_dates, mod_data_by_variable['apcp'])
        tmpmin.add_data(gcm_model, i, mod_dates, mod_data_by_variable['tmpmin'])
        tmpmax.add_data(gcm_model, i, mod_dates, mod_data_by_variable['tmpmax'])

    if output_dir is None:
        output_dir = '.'

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if verbose:
        print 'Summarizing precipitation...'
    with open(os.path.join(output_dir, 'pcp_stats.txt'), 'w') as f:
        f.write(pcp.stat_table(delimiter='\t'))  # alpha=0.05, bins=100, verbose=False

    if verbose:
        print 'Summarizing min. temperature...'
    with open(os.path.join(output_dir, 'tmpmin_stats.txt'), 'w') as f:
        f.write(tmpmin.stat_table(delimiter='\t')) # alpha=0.05, bins=100, verbose=False

    if verbose:
        print 'Summarizing max. temperature...'
    with open(os.path.join(output_dir, 'tmpmax_stats.txt'), 'w') as f:
        f.write(tmpmax.stat_table(delimiter='\t')) # alpha=0.05, bins=100, verbose=False


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Build statistics table for comparing modeled weather data to observed.')

    parser.add_argument('--sites_file', type=str, required=True, help='Path of the CSV file containing sites with columns: ({0}).'.format(','.join(SITE_COLUMNS)))
    parser.add_argument('--observed', type=str, required=True, help='Observed data source by name ({0}).'.format(','.join(CSIP_SOURCE_NAMES)))
    parser.add_argument('--modeled', type=str, nargs='+', required=True, help='List of modeled data sources by name ({0}).'.format(','.join(CSIP_SOURCE_NAMES)))

    parser.add_argument('--output_dir', type=str, default=None, help='Path to the output directory.')
    parser.add_argument('--start_date', type=str, default='1981-01-01', help='Start date in yyyy-mm-dd format. Default is "1981-01-01".')
    parser.add_argument('--end_date', type=str, default='2014-12-31', help='Ending date in yyyy-mm-dd format. Default is "2014-12-31".')
    parser.add_argument('--chunk_size', type=int, default=CHUNK_SIZE, help='Number of location downloaded at one time.')
    parser.add_argument('--climate_models', type=str, nargs='+', default=None, help='The MACA model number(s) or name(s)')
    parser.add_argument('--climate_rcps', type=str, nargs='+', default=None, help='The MACA RCP option(s) (rcp45 or rcp85)')

    args = parser.parse_args()

    data_start = datetime.strptime(args.start_date, '%Y-%m-%d')
    data_end = datetime.strptime(args.end_date, '%Y-%m-%d')
    save_stats(args.sites_file, args.observed, args.modeled, data_start, data_end, output_dir=args.output_dir,
               climate_models=args.climate_models, climate_rcps=args.climate_rcps, chunk_size=args.chunk_size, verbose=True)



