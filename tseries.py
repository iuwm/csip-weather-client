"""
Created on Dec 28, 2012

@author: Andre Dozier
"""
import csv
import time
import itertools
import numpy as np
from ast import literal_eval
from operator import itemgetter
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


DATE_20CENTURY = datetime(1900, 1, 1)
DATE_EPOCH = datetime(1970, 1, 1)


class Timestep:
    custom = -1
    daily = 0
    weekly = 1
    monthly = 2
    annual = 3

    def __init__(self, enum_val=-1, years=0, months=0, days=0, leapdays=0,
                 weeks=0, hours=0, minutes=0, seconds=0,
                 microseconds=0):

        self.preset = Timestep.__dict__.copy()
        self.preset.pop('custom')

        if isinstance(enum_val, Timestep):
            self.__dict__.update(enum_val.__dict__.copy())

        elif isinstance(enum_val, str):
            if enum_val in self.preset:
                self.enum_val = self.preset[enum_val]
                self.delta = {
                    0: relativedelta(days=1),
                    1: relativedelta(weeks=1),
                    2: relativedelta(months=1),
                    3: relativedelta(years=1)
                }[self.enum_val]
                self.ndays = 1 if self.enum_val == Timestep.daily else 0
            else:
                self.enum_val = -1
                args = literal_eval(enum_val)  # enumval is assumed to be a python dictionary
                self.delta = relativedelta(**args)
                self.ndays = self.days(DATE_20CENTURY) if years == 0 and months == 0 and leapdays == 0 else 0

        elif isinstance(enum_val, int) and enum_val >= 0:
            self.enum_val = enum_val
            self.delta = {
                0: relativedelta(days=1),
                1: relativedelta(weeks=1),
                2: relativedelta(months=1),
                3: relativedelta(years=1)
            }[enum_val]
            self.ndays = 1 if self.enum_val == Timestep.daily else 0

        else:
            self.enum_val = enum_val
            self.ndays = 0
            self.delta = relativedelta(years=years, months=months,
                                       days=days, leapdays=leapdays,
                                       weeks=weeks, hours=hours,
                                       minutes=minutes, seconds=seconds,
                                       microseconds=microseconds)

    def __cmp__(self, other):
        ts = Timestep(other)
        return int(self.enum_val).__cmp__(ts.enum_val)

    def is_finer_than(self, timestep):
        timestep = Timestep(timestep)
        return self.days(DATE_EPOCH) <= timestep.days(DATE_EPOCH)

    def increment(self, the_date):
        return the_date + self.delta

    def days(self, the_date):
        return date2num(the_date + self.delta) - date2num(the_date)

    def is_preset(self):
        if self.enum_val != -1:
            for k in self.preset:
                if self.preset[k] == self.enum_val:
                    return True
        return False

    def preset_string(self):
        if self.enum_val != -1:
            for k in self.preset:
                if self.preset[k] == self.enum_val:
                    return k
        return None

    def __repr__(self):
        if self.enum_val != -1:
            return self.preset_string()

        else:
            d = {'years': self.delta.years,
                 'months': self.delta.months,
                 'days': self.delta.days,
                 'leapdays': self.delta.leapdays,
                 'hours': self.delta.hours,
                 'minutes': self.delta.minutes,
                 'seconds': self.delta.seconds,
                 'microseconds': self.delta.microseconds}
            return str(d)


def aggregate_in_bins(input_dates, input_data, output_dates, fxn='sum', num_allowed_nans=0, fill_fxn='nan'):

    # instantiate variables
    output_len = len(output_dates)

    if input_data is None:
        raise ValueError('Input data cannot be NoneType!')

    if input_data.ndim > 1:

        if isinstance(fxn, str) or isinstance(fxn, unicode):
            fxn = [fxn] * input_data.shape[1]

        if len(fxn) != input_data.shape[1]:
            raise ValueError('Parameter "fxn" must have the same number of elements as the columns of data')

        out_data = np.zeros((output_len, input_data.shape[1]))
        nan_i = set()
        for i in range(input_data.shape[1]):
            out_data[:, i], nan_i_curr = aggregate_in_bins(input_dates, input_data[:, i], output_dates, fxn=fxn[i],
                                                           num_allowed_nans=num_allowed_nans, fill_fxn=fill_fxn)
            nan_i.update({i for i in nan_i_curr})
        nan_i = np.array(sorted(list(nan_i)), dtype=np.int64)

    else:

        # checks
        out_data = np.ones(output_len) * np.nan
        if isinstance(fxn, list) or isinstance(fxn, tuple):
            if len(fxn) != 1:
                raise ValueError('Parameter "fxn" must only have one element!')
            fxn = fxn[0]
        if len(input_data) == 0:
            return out_data, np.arange(output_len)

        # instantiate variables
        bins = np.digitize(input_dates, output_dates, right=False) - 1
        cnt = np.bincount(bins, minlength=output_len)
        i = slice(None)

        # allow nans in input dataset up to a certain number
        if num_allowed_nans > 0:
            nonnans = ~np.isnan(input_data)
            bins = bins[nonnans]
            input_data = input_data[nonnans]
            cnt_wo_nans = np.bincount(bins, minlength=len(cnt))
            i = cnt - cnt_wo_nans <= num_allowed_nans
            cnt = cnt_wo_nans[i]

        # get weighted sums
        col_data = np.bincount(bins, weights=input_data, minlength=output_len)[i]
        nonzero = cnt > 0
        if fxn == 'mean':
            out_data[i][nonzero] = col_data[nonzero] / cnt[nonzero]
        elif fxn == 'sum':
            out_data[i][nonzero] = col_data[nonzero]
        else:
            raise ValueError('Parameter "fxn" must be "sum" or "mean" or a list of such values')

        nans = np.isnan(out_data)
        nan_i = np.where(nans)[0]

        # fill missing data if possible
        if nans.any():
            if fill_fxn == 'step':
                # fill with the previous available data point

                # get locations of values just before nans
                nonnans = ~nans
                fill_val_i = np.where(np.logical_and(nonnans[:-1], nans[1:]))[0]

                # get fill values
                fill_vals = out_data[fill_val_i]

                # get fill value for each nan location
                bin_i = np.digitize(nan_i, fill_val_i, right=False) - 1
                if len(bin_i):
                    middle_vals_i = np.logical_and(bin_i != -1, bin_i != bin_i[-1])
                else:
                    middle_vals_i = bin_i != -1
                bin_i = bin_i[middle_vals_i]
                fill_vals = fill_vals[bin_i]

                # set nan locations with fill values
                out_data[nan_i[middle_vals_i]] = fill_vals

            elif fill_fxn.startswith('val_'):

                fill_val = float(fill_fxn[4:])
                out_data[nan_i] = fill_val

            elif fill_fxn != 'nan':
                raise ValueError('fill_fxn can only be "step", or "nan"! Got {0}'.format(fill_fxn))

    return out_data, nan_i


def dates2nums(dates):
    d2n = np.vectorize(date2num)
    return d2n(dates)


def date2num(date):
    return (date - DATE_20CENTURY).total_seconds() / 86400.0


def num2date(num_in_days):
    return DATE_20CENTURY + timedelta(days=num_in_days)


def nums2dates(nums_in_days):
    n2d = np.vectorize(num2date)
    return n2d(nums_in_days)


def struct2date(struct):
    return datetime(*struct[:6])


def structs2dates(structs):
    return [datetime(*t[:6]) for t in structs]


def date2struct(date):
    return date.timetuple()


def dates2structs(dates):
    return [d.timetuple() for d in dates]


def get_month(date):
    return date.month


def get_months(dates):
    gm = np.vectorize(get_month)
    return gm(dates)


def get_month_index(date, wateryearformat):
    if wateryearformat:
        return (date.month + 2) % 10
    else:
        return date.month - 1


def get_month_indices(dates, water_year_format):
    gm = np.vectorize(get_month_index)
    return gm(dates, water_year_format)


def fill_dates(start_date, end_date, timestep, date_objects=False):
    out_dates = []
    curr_date = start_date
    while curr_date < end_date:
        out_dates.append(curr_date)
        curr_date = timestep.increment(curr_date)
    if date_objects:
        return start_date, end_date, out_dates
    else:
        start_date = date2num(start_date)
        end_date = date2num(end_date)
        out_dates = dates2nums(out_dates)
        return start_date, end_date, out_dates


class TimeSeries:
    def __init__(self, dates=None, data=None):
        if dates is None:
            dates = []
        self.dates = dates
        if data is None:
            data = []
        self.data = np.array(data, dtype=np.float64)
        self._checks()
        self.nobs = None
        self.flags = None
        self.metadata = {}

    def copy(self):
        dates = self.dates[:] if isinstance(self.dates, list) else np.array(self.dates)
        data = self.data[:] if isinstance(self.data, list) else np.array(self.data)
        return TimeSeries(dates, data)

    def _checks(self):
        assert len(self.dates) == len(self.data), 'length of dates and data must equal'

    def size(self):
        return len(self.dates)

    def len(self):
        return len(self.dates)

    def __len__(self):
        return len(self.dates)

    def get_start_date(self):
        return self.dates[0] if len(self.dates) > 0 else None

    def get_end_date(self):
        return self.dates[-1] if len(self.dates) > 0 else None

    def get_time_structs(self):
        return [d.timetuple() for d in self.dates]

    def get_dates(self):
        return self.dates

    def get_year_day(self):
        return [(t.tm_year, t.tm_yday) for t in self.get_time_structs()]

    def get_days_since_1900(self):
        return dates2nums(self.dates)

    def get_timestamps(self):
        return np.int64([time.mktime(d.timetuple()) for d in self.dates])

    def get_year_month_day(self):
        return np.int32([d.timetuple()[:3] for d in self.dates])

    def set_dates_from_time_structs(self, times):
        self.dates = structs2dates(times)

    def set_dates_from_time_stamps(self, times):
        self.dates = nums2dates(times)

    def fill_dates(self, timestep, start_date=None, end_date=None, date_objects=False):

        timestep = Timestep(timestep)

        # use starting and ending dates for the timeseries
        if start_date is None:
            start_date = self.get_start_date()
        if end_date is None:
            end_date = self.get_end_date() + timedelta(microseconds=1)

        # get dates as floating-point numbers
        start_date, end_date, out_dates = fill_dates(start_date, end_date, timestep, date_objects=date_objects)
        return timestep, start_date, end_date, out_dates

    def aggregate(self, to_timestep, start_date=None, end_date=None, fxn='sum', num_allowed_nans=0, fill_fxn='nan'):

        # the timestep we're summing to
        to_timestep = Timestep(to_timestep)

        # dates and timestep variables
        timestep, start_date, end_date, out_dates = self.fill_dates(to_timestep, start_date, end_date)

        # truncate to time period
        self.dates = dates2nums(self.dates)
        time_i = np.logical_and(start_date <= self.dates, self.dates < end_date)
        self.dates = self.dates[time_i]

        if not isinstance(self.data, np.ndarray):
            self.data = np.array(self.data)
        self.data = self.data[time_i]

        # sum the output data
        input_dates = self.dates
        out_data, nan_i = aggregate_in_bins(input_dates, self.data, out_dates, fxn=fxn,
                                            num_allowed_nans=num_allowed_nans, fill_fxn=fill_fxn)

        # set output dates and nobs
        self.dates = nums2dates(out_dates)
        self.data = out_data

        if self.nobs is not None:
            if not isinstance(self.nobs, np.ndarray):
                self.nobs = np.array(self.nobs)
            self.nobs = self.nobs[time_i]
            out_nobs, _ = aggregate_in_bins(input_dates, self.nobs, out_dates, fxn=fxn,
                                            num_allowed_nans=num_allowed_nans, fill_fxn='val_0.0')
            self.nobs = np.array(out_nobs, dtype=np.int32)

        if self.flags is not None:
            if not isinstance(self.flags, np.ndarray):
                self.flags = np.array(self.flags)
            self.flags = list(self.flags[time_i])
            for row_i in nan_i:
                self.flags.insert(row_i, 'None')

    def indices_at_matching_dates(self, other):
        # returns (indices for this, indices for other)

        # get dates as numbers
        t = dates2nums(self.dates)
        ts = date2num(self.get_start_date())
        te = date2num(self.get_end_date())

        o = dates2nums(other.dates)
        os = date2num(other.get_start_date())
        oe = date2num(other.get_end_date())

        ti, = np.where(np.logical_and(os <= t, t <= oe))
        oi, = np.where(np.logical_and(ts <= o, o <= te))

        assert len(ti) == len(oi), 'Lengths of overlapping data do not match (data not sorted? different timesteps?)!'

        return ti, oi

    def data_at_matching_dates(self, other):
        # returns (data for this, data for other)

        ti, oi = self.indices_at_matching_dates(other)
        return self.data[ti], other.data[oi]


def date_from_values(values):
    values = list(int(v) for v in values)
    return datetime(*(values + [1] * (3 - len(values))))


class CategorizedTimeseries:

    date_cols = ['year', 'month']  # default is a monthly timeseries
    timestep = Timestep(Timestep.monthly)
    categories = ['geoid', 'category']
    variables = None

    def __init__(self, categories=None, variables=None, date_cols=None, timestep=None):
        if timestep is not None:
            self.timestep = timestep
        if date_cols is not None:
            self.date_cols = date_cols
        if categories is not None:
            self.categories = categories
        if variables is not None:
            self.variables = variables
        self.keys = []
        self.key_i = -1
        self.data = {}

    def add(self, key, child_data):
        if key in self.data:
            raise ValueError('Data contains key {0} already!'.format(key))
        self.keys.append(key)
        self.data[key] = child_data

    def __getitem__(self, key):
        return self.data[key]

    def __iter__(self):
        return self

    def __contains__(self, key):
        return key in self.data

    def next(self):
        self.key_i += 1
        if self.key_i >= len(self.keys):
            self.key_i = -1  # reset
            raise StopIteration
        else:
            return self.keys[self.key_i]

    def create_child(self, categories):
        child = CategorizedTimeseries()
        child.categories = categories
        child.variables = self.variables
        child.date_cols = self.date_cols
        child.timestep = self.timestep
        return child

    def date_from_row(self, row):
        date_values = [int(row[d]) for d in self.date_cols]
        return date_from_values(date_values)

    def read_data(self, data):
        # build data
        if self.categories:

            sorted_data = sorted(data, key=lambda r: r[self.categories[0]])
            for key, child_data in itertools.groupby(sorted_data, key=lambda r: r[self.categories[0]]):
                child = self.create_child(self.categories[1:])
                child.read_data(child_data)
                if key in self.keys:
                    raise Exception('key {0} is somehow in the child keys {1}!'.format(key, ', '.join(self.keys)))
                self.add(key, child)

        else:

                def loop_dict():
                    dates = [date_from_values(date_values) for date_values in zip(*[data[dc] for dc in self.date_cols])]
                    for output_variable in self.variables:
                        values = data[output_variable]
                        yield output_variable, dates, values

                def loop_list():
                    ts_sorted = sorted(data, key=itemgetter(*self.date_cols))
                    dates = [self.date_from_row(row) for row in ts_sorted]
                    for output_variable in self.variables:
                        values = np.array([row[output_variable] for row in ts_sorted])
                        yield output_variable, dates, values

                gen = loop_dict() if isinstance(data, dict) else loop_list()
                for var, d, v in gen:
                    ts = TimeSeries(d, v)
                    ts.aggregate(self.timestep)
                    self.add(var, ts)

        return self

    def read_file(self, csv_file):

        # read csv file
        with open(csv_file, 'rb') as f:
            cr = csv.DictReader(f)

            # if no output variables specified, return all
            if self.variables is None:
                self.variables = [h for h in cr.fieldnames if h not in self.categories + self.date_cols]

            # required columns
            all_cols = self.categories + self.date_cols + self.variables

            # check for categories and output variables in file
            for h in all_cols:
                if h not in cr.fieldnames:
                    msg = 'Header {0} not found in csv file {1}! Headers found: {2}'
                    raise ValueError(msg.format(h, csv_file, ', '.join(cr.fieldnames)))

            # fill list with data
            data = [row for row in cr]
        return self.read_data(data)

    @classmethod
    def from_file(cls, csv_file, categories=None, variables=None, date_cols=None, timestep=None):

        self = cls(categories=categories, variables=variables, date_cols=date_cols, timestep=timestep)
        return self.read_file(csv_file)
