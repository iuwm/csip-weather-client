import os
import sys
import json
import requests
import numpy as np
from collections import OrderedDict
from weather import SITE_COLUMNS, CSIP_SOURCE_NAMES, read_sites_file, save_sites_file, \
    HEADER_MAP, iter_files
from tseries import TimeSeries, Timestep
from datetime import datetime


USGS_NED_URL = r'http://ned.usgs.gov/epqs/pqs.php?x={longitude}&y={latitude}&units=Meters&output=json'


def batch_stats(the_dir, sites_file, source=None, out_dir=None):

    # initial variables
    site_info = {}
    extra_headers = []
    print '\n'

    # find each climate file
    for f in iter_files(the_dir, '*.csv'):

        # get file info
        file_name = os.path.splitext(os.path.basename(f))[0]
        file_split_i = file_name.find(',')

        # verbosity
        sys.stdout.write('\rWorking on {0:<50s}'.format(file_name))
        sys.stdout.flush()

        # read the data
        with open(f, 'r') as fh:
            headers = [str(c).strip() for c in fh.readline().split(',')]
            data = np.loadtxt(fh, delimiter=',')

        # get the number of observations
        num_data = data[:, 4:]
        nobs = np.sum(~np.isnan(num_data), axis=0)
        nobs = OrderedDict([(HEADER_MAP[headers[4 + i]], nobs[i]) for i in range(len(nobs))])
        if len(nobs) > len(extra_headers):
            extra_headers = nobs.keys()

        # get annual statistics
        dates = [datetime(int(d[2]), int(d[1]), int(d[0])) for d in data]
        tmin = TimeSeries(dates, data[:, 4])
        tmin.aggregate(Timestep.annual, fxn='mean', num_allowed_nans=36)
        tmax = TimeSeries(dates, data[:, 5])
        tmax.aggregate(Timestep.annual, fxn='mean', num_allowed_nans=36)
        tavg = TimeSeries(dates, data[:, 4:6].mean(axis=1))
        tavg.aggregate(Timestep.annual, fxn='mean', num_allowed_nans=36)
        apcp = TimeSeries(dates, data[:, 6])
        apcp.aggregate(Timestep.annual, fxn='sum', num_allowed_nans=36)

        # build site info dict
        curr_site_info = {
            'id': file_name[:file_split_i].strip(),
            'name': file_name[file_split_i+1:].strip(),
            'headers': headers,
            'data': data,
            'nobs': nobs,
            'apcp': np.nanmean(apcp.data),
            'tmin': np.nanmean(tmin.data),
            'tmax': np.nanmean(tmax.data),
            'tavg': np.nanmean(tavg.data)
        }

        site_info[file_name] = curr_site_info

    print '\n'

    if sites_file is not None:

        out_sites_file = os.path.splitext(sites_file)[0] + '_va.csv'
        if out_dir is not None:
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            out_sites_file = os.path.join(out_dir, os.path.basename(out_sites_file))
        print '\n\nWriting sites file {0}...\n\n'.format(out_sites_file)
        sites = read_sites_file(sites_file, source=source)

        for site in sites:
            key = '{0},{1}'.format(site['id'], site['name'])
            key = key.lower()
            if key in site_info:
                nobs = site_info[key]['nobs']
                for k in extra_headers:
                    if k in nobs:
                        site['nobs ' + k] = nobs[k]
                    else:
                        site['nobs ' + k] = 0
                site['Annual Average Precipitation (cm)'] = site_info[key]['apcp']
                site['Annual Average Daily Min. Temperature (C)'] = site_info[key]['tmin']
                site['Annual Average Daily Max. Temperature (C)'] = site_info[key]['tmax']
                site['Annual Average Temperature (C)'] = site_info[key]['tavg']
            else:
                for k in extra_headers:
                    site['nobs ' + k] = 0
                site['Annual Average Precipitation (cm)'] = np.nan
                site['Annual Average Daily Min. Temperature (C)'] = np.nan
                site['Annual Average Daily Max. Temperature (C)'] = np.nan
                site['Annual Average Temperature (C)'] = np.nan

            # get elevation (in meters)
            elev_resp = requests.get(USGS_NED_URL.format(longitude=site['longitude'], latitude=site['latitude']))
            elev = json.loads(elev_resp.text)["USGS_Elevation_Point_Query_Service"]["Elevation_Query"]["Elevation"]
            site['Elevation (m)'] = elev

        save_sites_file(out_sites_file, sites, overwrite=True)

    return site_info


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='Get stats for downloaded weather files.')
    parser.add_argument('dir', type=str, help='Directory containing the downloaded weather files')
    parser.add_argument('sites_file', type=str,
                        help='Path of CSV file containing sites with columns: ({0}).'.format(','.join(SITE_COLUMNS)))
    parser.add_argument('--source', type=str, default=None,
                        help='Data source name ({0}).'.format(', '.join(CSIP_SOURCE_NAMES)))
    parser.add_argument('--out_dir', type=str, default=None, help='The output directory')
    args = parser.parse_args()

    batch_stats(args.dir, args.sites_file, source=args.source, out_dir=args.out_dir)
