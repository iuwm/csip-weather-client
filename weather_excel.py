import os
import numpy as np
from datetime import datetime
from openpyxl import Workbook
from tseries import TimeSeries, Timestep
from weather import HEADER_MAP, iter_files, MODEL_VARIABLE_AGGREGATION


def to_excel(the_dir, output_file, start_date, end_date, timestep=Timestep.daily):

    output_data = {}
    filled_dates = None
    for data_file in iter_files(the_dir, '*.csv'):
        station = os.path.basename(os.path.splitext(data_file)[0])
        print 'Working on {0}..'.format(station)
        with open(data_file, 'r') as f:
            variables = [str(c).strip() for c in f.readline().split(',')][4:]
            data = np.loadtxt(f, delimiter=',')
        dates = [datetime(int(d[2]), int(d[1]), int(d[0])) for d in data]
        num_data = data[:, 4:]
        std_variables = [HEADER_MAP[v] for v in variables]
        ts = TimeSeries(dates, num_data)
        ts.aggregate(timestep, start_date=start_date, end_date=end_date,
                     fxn=MODEL_VARIABLE_AGGREGATION[:ts.data.shape[1]])
        if filled_dates is None:
            filled_dates = ts.dates
        for i, v in zip(range(ts.data.shape[1]), std_variables):
            curr_data = ts.data[:, i:i+1]
            if (~np.isnan(curr_data)).sum() == 0:
                continue  # skip when there is no data
            if v in output_data:
                output_data[v]['data'] = np.hstack((output_data[v]['data'], curr_data))
                output_data[v]['stations'].append(station)
            else:
                output_data[v] = {
                    'data': curr_data,
                    'stations': [station]
                }
    if filled_dates is None:
        raise ValueError('No data was found.')

    print 'Creating workbook...'
    wb = Workbook(write_only=True)

    for k in output_data:
        title = k.replace('/', ' per ')
        print 'Adding sheet {0}...'.format(title)
        ws = wb.create_sheet(title=title)
        ws.append(['Date'] + [str(s) for s in output_data[k]['stations']])
        for date, row in zip(filled_dates, output_data[k]['data']):
            # date_str = date.strftime('%Y-%m-%d')
            # ws.append([date_str] + [str(s) for s in row.tolist()])
            ws.append([date] + row.tolist())

    print 'Saving to {0}...'.format(output_file)
    wb.save(filename=output_file)
    return output_data


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='Convert weather downloads to Excel format.')
    parser.add_argument('dir', type=str, help='Directory where weather downloads reside.')
    parser.add_argument('output_file', type=str, help='Output Excel file.')
    parser.add_argument('start_date', type=str, help='Start date in yyyy-mm-dd format.')
    parser.add_argument('end_date', type=str, help='End date in yyyy-mm-dd format.')
    parser.add_argument('--timestep', type=str, default='daily', help='Timestep at which to fill output.')
    args = parser.parse_args()

    args_start_date = datetime.strptime(args.start_date, '%Y-%m-%d')
    args_end_date = datetime.strptime(args.end_date, '%Y-%m-%d')
    to_excel(args.dir, args.output_file, args_start_date, args_end_date, timestep=args.timestep)
