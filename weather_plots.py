import os
import weather
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt


def plot_time_coverage(sites_file, start_date, end_date, output_dir='output', data_dir=None, use_cache=False,
                       sources=None, climate_models=None, climate_rcps=None,
                       aggregate_timestep=None):

    print 'Retrieving data...'
    outputs = weather.WeatherOutputs(sites_file, start_date, end_date, output_dir=data_dir, use_cache=use_cache,
                                     sources=sources, climate_models=climate_models, climate_rcps=climate_rcps,
                                     aggregate_timestep=aggregate_timestep)

    n = outputs.size()
    print 'Got {0} outputs!'.format(n)
    if n == 0:
        return

    output_dir = os.path.join(output_dir, os.path.splitext(os.path.basename(sites_file))[0])
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    print 'Output directory {0}...'.format(output_dir)

    var_names = next(wo for wo in outputs.site_data).variables
    for var_name in var_names:
        print 'Plotting {0}...'.format(var_name)

        def can_plot(wo):
            return var_name in wo.variables and not np.isnan(wo.get_data(var_name)).all()

        plt.figure(figsize=(8, 5))
        plt.yticks([])

        models_to_plot = [wo for wo in outputs.site_data if can_plot(wo)]
        n_models_to_plot = len(models_to_plot)
        plt.ylim([0, n_models_to_plot + 1])

        for i, wo in zip(range(n_models_to_plot), models_to_plot):

            data = np.ones(len(wo.ts.dates)) * (i + 1)
            nan_i = np.isnan(wo.get_data(var_name))
            data[nan_i] = np.nan
            plt.plot_date(wo.ts.dates, data, 'k-', lw=5)

            data = np.ones(len(wo.ts.dates)) * (i + 1)
            indices = np.where(~nan_i)[0]
            if len(indices):
                start_i = indices[0]
                end_i = indices[-1]
                data[:start_i] = np.nan
                data[end_i+1:] = np.nan
            data[~nan_i] = np.nan
            plt.plot_date(wo.ts.dates, data, 'ro', markeredgecolor='none', ms=3)

        # tighten
        plt.tight_layout()

        # move the axis over to the right a bit
        ax = plt.gca()
        pos1 = ax.get_position()  # get the original position
        move_dist = 0.15
        pos2 = [pos1.x0 + move_dist, pos1.y0,  pos1.width - move_dist, pos1.height]
        ax.set_position(pos2)  # set a new position

        # place labels to the left of the axis
        xlim = plt.xlim()
        width = xlim[1] - xlim[0]
        for i, wo in zip(range(n_models_to_plot), models_to_plot):
            plt.text(xlim[0] - 0.05 * width, i + 1, '{0}: {1}'.format(wo.source, wo.site['id']),
                     va='center', ha='right', fontsize=6)

        plt.savefig(os.path.join(output_dir, var_name + '.png'))


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Plot weather data.')

    parser.add_argument('--sites_file', type=str, required=True, help='Path of the CSV file containing sites with columns: ({0}).'.format(','.join(weather.SITE_COLUMNS)))
    parser.add_argument('--sources', type=str, default=None, help='List of modeled data sources by name ({0}).'.format(','.join(weather.CSIP_SOURCE_NAMES)))

    parser.add_argument('--output_dir', type=str, default='output', help='Path to the output directory.')
    parser.add_argument('--data_dir', type=str, default=None, help='Specifies whether or not to use the cache.')
    parser.add_argument('--cache', action='store_true', help='Specifies whether or not to use the cache.')

    parser.add_argument('--start_date', type=str, default='1981-01-01', help='Start date in yyyy-mm-dd format. Default is "1981-01-01".')
    parser.add_argument('--end_date', type=str, default='2014-12-31', help='Ending date in yyyy-mm-dd format. Default is "2014-12-31".')
    parser.add_argument('--climate_models', type=str, nargs='+', default=weather.MODEL_NAMES, help='The MACA model number(s) or name(s)')
    parser.add_argument('--climate_rcps', type=str, nargs='+', default=weather.MODEL_RCPS, help='The MACA RCP option(s) (rcp45 or rcp85)')

    parser.add_argument('--aggregate_timestep', type=str, default=None, help='Timestep at which to aggregate the data (e.g., "daily", "monthly", "annual")')

    args = parser.parse_args()

    data_start = datetime.strptime(args.start_date, '%Y-%m-%d')
    data_end = datetime.strptime(args.end_date, '%Y-%m-%d')
    plot_time_coverage(args.sites_file, data_start, data_end, output_dir=args.output_dir,
                       data_dir=args.data_dir, use_cache=args.cache, sources=args.sources,
                       climate_models=args.climate_models, climate_rcps=args.climate_rcps,
                       aggregate_timestep=args.aggregate_timestep)

